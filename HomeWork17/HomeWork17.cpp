#include <iostream>
using namespace std;

class vector
{

public:
    vector(): x(0), y(0), z(0)
    {}
    vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}
    void show()
    {
        cout << x << " " << y << " " << z << endl;
    }
    void mod()
    {
        cout << sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2)) << endl;
    }
   
private:
    double x;
    double y;
    double z;
    
};

int main()
{

    vector v(10, 10, 10);
    v.show();

    vector j(3, 3.3 , 4.4);
    j.mod();
    
}

